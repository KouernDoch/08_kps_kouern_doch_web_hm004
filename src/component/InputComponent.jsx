import { Component } from "react";
import TableComponent from "./TableComponent";
import Swal from 'sweetalert2';
import "animate.css"


class InputComponent extends Component{
constructor(){
super();
this.state={
  person:[
    {
      id:1,
      email:"doch@gmail.com",
      username:"doch",
      age:21,
      status:"Pending"}
  ],
  newEmail:"null",newUsername:"null",newAge:"null",newStatus: "Pending"
}
}
Alert=(id)=>{
  this.state.person.map((ps)=>{
    if(ps.id==id){
      Swal.fire({
        title: "id: "+ id +
               "\nEmail : "+ps.email+
               "\nUsername : "+ps.username+
               "\nAge : "+ps.age,
        width: 600,
        padding: '3em',
        color: '#716add',
        backdrop: `
          rgba(0,0,123,0.4)
          left top
          no-repeat`
      });
    }
  })
  }
ChangStatus=(id)=>{
  this.state.person.map((ps)=>{
    if(ps.id==id){
      ps.status=="Pending"?(ps.status="Done"):(ps.status="Pending")
    }
  })
  this.setState({
    person:this.state.person
  })
}
EmailChange=(event) => {
this.setState({
  newEmail:event.target.value
})
}
UserChange=(event) => {
  this.setState({
    newUsername: event.target.value
  })
  }
  AgeChange=(event) => {
    this.setState({
      newAge: event.target.value
    })
    }

register=()=>{
  const newobj={
    id: this.state.person.length+1,
    email: this.state.newEmail,
    username: this.state.newUsername,
    age:this.state.newAge,
    status:this.state.newStatus
  }
  this.setState({
    person:[...this.state.person,newobj],newEmail:"null",newUsername:"null",newAge:"null",newStatus:"Pending"
  })
}

render(){
return(
<div className="InputInfor">
<p className="text-6xl font-bold pt-14 text-emerald-900">Please fill you infomation</p>

  <div className="mb-4 my-10 w-[70%] mx-auto">
  <label className="block text-left text-xl font-bold mb-2 mx-0" for="email"> Your Email </label>
<div class="relative">
  <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
    <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
  </div>
  <input type="text" id="email-address-icon" class="text-xl rounded-lg block w-full pl-10 p-3  dark:placeholder-gray-400 dark:text-black" placeholder="name@gmail.com"
  onChange={this.EmailChange}
  />
</div>
</div>

<div className="mb-4 my-23 w-[70%] mx-auto">
<label for="age" class="block text-left text-xl font-bold mb-2">Username</label>
<div class="flex">
  <span class="inline-flex items-center px-3 text-sm bg-gray-200 border border-r-0 border-gray-300 rounded-l-md  dark:text-gray-400 ">
    @
  </span>
  <input type="text" id="age" class=" rounded-r-lg  block flex-1 min-w-0 w-full text-xl p-3 text-black" placeholder="kouern doch"
  onChange={this.UserChange}
  />
</div>
</div>
    <div className="mb-4 my-23 w-[70%] mx-auto">
        <label for="age" class="block text-left text-xl font-bold mb-2">Age</label>
          <div class="flex">
             <span class="inline-flex items-center px-3 text-sm bg-gray-200 border border-r-0 border-gray-300 rounded-l-md  dark:text-gray-400 ">
    ❤️
             </span>
      <input class=" rounded-r-lg  block flex-1 min-w-0 w-full text-xl p-3 text-black" placeholder="20" type="text" id="age"
        onChange={this.AgeChange}
      />
      </div>
    </div>
  <button onClick={this.register} className=" bg-transparent my-10 bg-white hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-3 px-40 border border-blue-500 hover:border-transparent rounded-lg">
  Register
  </button>

  <TableComponent data={this.state.person} ChangStatus={this.ChangStatus} Alert={this.Alert}/>

</div>  
)
}
}
export default InputComponent;   