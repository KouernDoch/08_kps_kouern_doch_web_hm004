import PropTypes from 'prop-types'
import { Component } from "react";
import Swal from 'sweetalert2';
import "animate.css"

class TableComponent extends Component{

  constructor(){
    super();
}
  render() {
    return (
        
<div class="relative overflow-x-auto">
    <table class="w-[70%] mx-auto text-sm text-left text-gray-500 dark:text-gray-400">
        <thead class="text-xl text-black uppercase ">
            <tr>
                <th scope="col" class="px-6 py-3 text-center">
                    ID
                </th>
                <th scope="col" class="px-6 py-3 text-center">
                   Email
                </th>
                <th scope="col" class="px-6 py-3 text-center">
                    UserName
                </th>
                <th scope="col" class="px-6 py-3">
                    Age
                </th>
                <th scope="col" class="px-6 py-3 text-center">
                    Action
                </th>
            </tr>
        </thead>
        <tbody>
            {this.props.data.map((item)=>(
                <tr className={`border-b darkbg-white  dark:border-gray-700 text-black text-xl ${(item.id)%2 === 0 ? "bg-red-200" :"bg-white"}`}>
                    <td className="px-6 py-4 text-center">{item.id}</td>
                    <td className="px-6 py-4 text-center">{item.email}</td>
                    <td className="px-6 py-4 text-center">{item.username}</td>
                    <td className="px-6 py-4 text-center">{item.age}</td>
                    <td className="px-6 py-4 pl-40">
                        <button onClick={()=>this.props.ChangStatus(item.id)} type="button" class={`focus:outline-none text-white font-medium rounded-lg text-sm px-8 py-2.5 mr-2 mb-2 ${item.status === "Pending" ? "bg-red-600" : "bg-green-500" }`}>{item.status}</button>
                        <button onClick={()=>this.props.Alert(item.id)} type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-8 py-2.5 mr-2 mb-2  dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Show more</button>
                    </td>
                </tr>
            ))}
        </tbody>
    </table>
</div>
    )
  }
}
export default TableComponent;  
