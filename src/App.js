import './App.css';
import InputComponent from './component/InputComponent';

function App() {
  return (
    <div className="App bg-teal-100  min-h-screen">
    <InputComponent></InputComponent>
    </div>
  );
}

export default App;
